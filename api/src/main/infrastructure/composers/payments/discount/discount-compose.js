const DiscountRouter = require('../../../../../payments/application/discount/router/discount-router')
const DiscountUseCase = require('../../../../../payments/domain/discount/useCase/discount-useCase')
const EmailValidator = require('../../../../application/emailValidator/utils/email-validator')
const LoadUserByEmailAndStoreRepository = require('../../../../../payments/infrastructure/load/repositories/loadPayment-repository')
const WriteUserByEmailAndStoreRepository = require('../../../../../payments/infrastructure/write/repositories/writePayment-repository')

module.exports = class DiscountRouterCompose {
  static compose () {
    const emailValidator = new EmailValidator()
    const loadUserbyEmailAndStoreRepository = new LoadUserByEmailAndStoreRepository()
    const writeUserbyEmailAndStoreRepository = new WriteUserByEmailAndStoreRepository()
    const discountUseCase = new DiscountUseCase({ loadUserbyEmailAndStoreRepository, writeUserbyEmailAndStoreRepository })
    return new DiscountRouter({ discountUseCase, emailValidator })
  }
}
