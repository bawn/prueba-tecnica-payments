const AddRouter = require('../../../../../payments/application/add/router/add-router')
const AddUseCase = require('../../../../../payments/domain/add/useCase/add-useCase')
const EmailValidator = require('../../../../application/emailValidator/utils/email-validator')
const LoadUserByEmailAndStoreRepository = require('../../../../../payments/infrastructure/load/repositories/loadPayment-repository')
const WriteUserByEmailAndStoreRepository = require('../../../../../payments/infrastructure/write/repositories/writePayment-repository')

module.exports = class AddRouterCompose {
  static compose () {
    const emailValidator = new EmailValidator()
    const loadUserbyEmailAndStoreRepository = new LoadUserByEmailAndStoreRepository()
    const writeUserbyEmailAndStoreRepository = new WriteUserByEmailAndStoreRepository()
    const addUseCase = new AddUseCase({ loadUserbyEmailAndStoreRepository, writeUserbyEmailAndStoreRepository })
    return new AddRouter({ addUseCase, emailValidator })
  }
}
