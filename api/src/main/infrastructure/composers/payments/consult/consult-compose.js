const ConsultRouter = require('../../../../../payments/application/consult/router/consult-router')
const ConsultUseCase = require('../../../../../payments/domain/consult/useCase/consult-useCase')
const EmailValidator = require('../../../../application/emailValidator/utils/email-validator')
const LoadUserByEmailAndStoreRepository = require('../../../../../payments/infrastructure/load/repositories/loadPayment-repository')

module.exports = class ConsultRouterCompose {
  static compose () {
    const emailValidator = new EmailValidator()
    const loadUserbyEmailAndStoreRepository = new LoadUserByEmailAndStoreRepository()
    const consultUseCase = new ConsultUseCase({ loadUserbyEmailAndStoreRepository })
    return new ConsultRouter({ consultUseCase, emailValidator })
  }
}
