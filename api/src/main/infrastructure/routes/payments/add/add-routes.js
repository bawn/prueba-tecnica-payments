const AddRouterCompose = require('../../../composers/payments/add/add-compose')
const { adapt } = require('../../../adapters/express/express-router-adapter')

module.exports = router => {
  router.put('/payments/add', adapt(AddRouterCompose.compose()))
}
