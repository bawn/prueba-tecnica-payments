const ConsultRouterCompose = require('../../../composers/payments/consult/consult-compose')
const { adapt } = require('../../../adapters/express/express-router-adapter')

module.exports = router => {
  router.post('/payments/consult', adapt(ConsultRouterCompose.compose()))
}
