const DiscountRouterCompose = require('../../../composers/payments/discount/discount-compose')
const { adapt } = require('../../../adapters/express/express-router-adapter')

module.exports = router => {
  router.put('/payments/discount/', adapt(DiscountRouterCompose.compose()))
}
