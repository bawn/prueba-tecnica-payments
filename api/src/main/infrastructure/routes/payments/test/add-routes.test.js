const request = require('supertest')
const app = require('../../../config/app-config')

describe('Add Route', () => {
  test('Should return 200 when valid data are provided', async () => {
    await request(app)
      .put('/api/payments/add')
      .send({ email: 'juan@perez.cl', amount: 1000, store: 'A' })
      .expect(200)
  })
})
