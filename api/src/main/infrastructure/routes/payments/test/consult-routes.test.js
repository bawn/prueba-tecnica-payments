const request = require('supertest')
const app = require('../../../config/app-config')

describe('Consult Route', () => {
  test('Should return 200 when valid data are provided', async () => {
    await request(app)
      .post('/api/payments/consult')
      .send({ email: 'juan@perez.cl', store: 'A' })
      .expect(200)
  })
})
