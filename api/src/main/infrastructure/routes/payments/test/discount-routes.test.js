const request = require('supertest')
const app = require('../../../config/app-config')

describe('Discount Route', () => {
  test('Should return 200 when valid data are provided', async () => {
    await request(app)
      .put('/api/payments/discount')
      .send({ email: 'juan@perez.cl', amount: 1000, store: 'A' })
      .expect(200)
  })
})
