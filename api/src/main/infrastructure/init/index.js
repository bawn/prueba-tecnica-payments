const app = require('../config/app-config')
const port = process.env.PORT || 3000
require('dotenv').config()

app.listen(port, () => {
  console.log(`puerto: ${port}`)
})
