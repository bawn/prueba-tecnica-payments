const cors = require('../middlewares/cors-middleware')
const contentType = require('../middlewares/content-type-middleware')
const jsonParser = require('../middlewares/json-parser')

module.exports = app => {
  app.disable('x-powered-by')
  app.use(cors)
  app.use(jsonParser)
  app.use(contentType)
}
