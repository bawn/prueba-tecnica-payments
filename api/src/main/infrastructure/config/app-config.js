const express = require('express')
const app = express()
const setupApp = require('./setup-config')
const setupRoutes = require('./routes-config.js')

setupApp(app)
setupRoutes(app)

module.exports = app
