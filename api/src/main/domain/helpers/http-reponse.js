const { ServerError } = require('../errors/index')

module.exports = class httpResponse {
  static badRequest (error) {
    return {
      statusCode: 400,
      body: error
    }
  }

  static serverError () {
    return {
      statusCode: 500,
      body: new ServerError()
    }
  }

  static Ok (data) {
    return {
      statusCode: 200,
      body: data.message
    }
  }
}
