const WriteUserByEmailAndStoreRepository = require('../repositories/writePayment-repository')
const LoadUserByEmailAndStoreRepository = require('../../load/repositories/loadPayment-repository')

describe('Load payment by Emay and Store Repository', () => {
  test('should return null if no user if found', async () => {
    const sut = new LoadUserByEmailAndStoreRepository()
    const user = await sut.load('invalid@perez.cl', 'A')
    expect(user).toBeNull()
  })

  test('should return an user if found', async () => {
    const sut = new LoadUserByEmailAndStoreRepository()
    const user = await sut.load('juan@perez.cl', 'A')
    expect(user.email).toBe('juan@perez.cl')
    expect(user.store).toBe('A')
  })

  test('should return an user if found and write file with new data', async () => {
    const req = {
      email: 'juan@perez.cl',
      store: 'A',
      amount: 3500
    }
    const sutWrite = new WriteUserByEmailAndStoreRepository()
    const write = await sutWrite.write(req)
    expect(req).toBe(write)
  })
})
