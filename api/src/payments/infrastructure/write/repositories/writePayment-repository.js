const CsvLoadHelper = require('../../load/helpers/csv-load-helper')
const CsvWriteHelper = require('../helpers/csv-writter-helper')

module.exports = class WriteUserByEmailAndStoreRepository {
  async write (user) {
    const client = await CsvLoadHelper.csv_load()
    const data = await client('./csv/entrada.csv')

    for (let i = 0; i < data.length; i++) {
      if (data[i].email === user.email && data[i].store === user.store) {
        data[i] = user
      }
    }

    await CsvWriteHelper.csv_writer('./csv/entrada.csv', data)
    return user
  }
}
