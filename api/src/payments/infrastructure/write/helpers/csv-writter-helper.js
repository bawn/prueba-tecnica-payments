const createCsvWriter = require('csv-writer').createObjectCsvWriter

module.exports = {
  async csv_writer (route, data) {
    const csvWriter = createCsvWriter({
      path: route,
      header: [
        { id: 'email', title: 'correo' },
        { id: 'store', title: 'tienda' },
        { id: 'amount', title: 'monto' }
      ]
    })
    await csvWriter
      .writeRecords(data)
  }
}
