const fs = require('fs')
const csv = require('fast-csv')

module.exports = {
  async csv_load () {
    const data = (route) => new Promise((resolve) => {
      const payments = []
      return fs.createReadStream(route)
        .pipe(csv.parse({ headers: true }))
        .on('error', error => console.error(error))
        .on('data', row => {
          payments.push({
            email: row.correo,
            amount: row.monto,
            store: row.tienda
          })
        })
        .on('end', () => resolve(payments))
    }).then(res => { return res })

    return data
  }
}
