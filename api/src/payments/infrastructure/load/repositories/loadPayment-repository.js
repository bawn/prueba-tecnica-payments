const CsvHelper = require('../helpers/csv-load-helper')

module.exports = class LoadUserByEmailAndStoreRepository {
  async load (email, store) {
    const client = await CsvHelper.csv_load()
    const data = await client('./csv/entrada.csv')

    const paymentUser = data.find(el => {
      if (el.email === email && el.store === store) {
        return el
      }
      return null
    })

    if (!paymentUser) {
      return null
    }

    return paymentUser
  }
}
