const LoadUserByEmailAndStoreRepository = require('../repositories/loadPayment-repository')

describe('Load payment by Emay and Store Repository', () => {
  test('should return null if no user if found', async () => {
    const sut = new LoadUserByEmailAndStoreRepository()
    const user = await sut.load('invalid@perez.cl', 'A')
    expect(user).toBeNull()
  })

  test('should return an user if found', async () => {
    const sut = new LoadUserByEmailAndStoreRepository()
    const user = await sut.load('juan@perez.cl', 'A')
    expect(user.email).toBe('juan@perez.cl')
    expect(user.store).toBe('A')
  })
})
