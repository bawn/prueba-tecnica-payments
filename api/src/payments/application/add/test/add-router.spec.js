const AddRouter = require('../router/add-router')
const { ServerError, MissingParamError, InvalidParamError } = require('../../../../main/domain/errors/index')

const makeSut = () => {
  const addUseCaseSpy = makeAddUseCase()
  const emailValidatorSpy = makeEmailValidator()
  const sut = new AddRouter({
    addUseCase: addUseCaseSpy,
    emailValidator: emailValidatorSpy
  })
  return { sut, addUseCaseSpy, emailValidatorSpy }
}

const makeAddUseCase = () => {
  class AddUseCaseSpy {
    async add (email, amount, store) {
      this.email = email
      this.amount = amount
      this.store = store

      this.menssage = {
        email, amount, store
      }

      return this.menssage
    }
  }

  return new AddUseCaseSpy()
}

const makeAddUseCaseError = () => {
  class AddUseCaseSpy {
    async add () {
      throw new Error()
    }
  }

  return new AddUseCaseSpy()
}

const makeEmailValidator = () => {
  class EmailValidatorSpy {
    isValid (email) {
      this.email = email
      return this.isEmailValid
    }
  }

  const emailValidatorSpy = new EmailValidatorSpy()
  emailValidatorSpy.isEmailValid = true
  return emailValidatorSpy
}

const makeEmailValidatorError = () => {
  class EmailValidatorSpy {
    isValid () {
      throw new Error()
    }
  }

  return new EmailValidatorSpy()
}

describe('Add Router', () => {
  test('Should returnm 200 Added amount', async () => {
    const { sut, addUseCaseSpy } = makeSut()
    const httpRequest = {
      body: {
        email: 'juan@perez.cl',
        amount: 2000,
        store: 'A'
      }
    }
    const httpResponse = await sut.route(httpRequest)
    expect(httpResponse.statusCode).toBe(200)
    expect(httpResponse.body).toEqual(addUseCaseSpy.menssage)
  })

  test('Should call EmailValidator with correct email', async () => {
    const { sut, emailValidatorSpy } = makeSut()
    const httpRequest = {
      body: {
        email: 'juan@perez.cl',
        amount: 2000,
        store: 'A'
      }
    }
    await sut.route(httpRequest)
    expect(emailValidatorSpy.email).toBe(httpRequest.body.email)
  })

  test('Should call AuthUseCase with correct params', async () => {
    const { sut, addUseCaseSpy } = makeSut()
    const httpRequest = {
      body: {
        email: 'any_email@email.com',
        amount: 2000,
        store: 'A'
      }
    }
    await sut.route(httpRequest)
    expect(addUseCaseSpy.email).toBe(httpRequest.body.email)
    expect(addUseCaseSpy.amount).toBe(httpRequest.body.amount)
    expect(addUseCaseSpy.store).toBe(httpRequest.body.store)
  })

  test('Should return 400 if no email is provide', async () => {
    const { sut } = makeSut()
    const httpRequest = {
      body: {
        amount: 2000,
        store: 'A'
      }
    }
    const httpResponse = await sut.route(httpRequest)

    expect(httpResponse.statusCode).toBe(400)
    expect(httpResponse.body).toEqual(new MissingParamError('email'))
  })

  test('Should return 400 if no amount is provide', async () => {
    const { sut } = makeSut()
    const httpRequest = {
      body: {
        email: 'any_password@email.com',
        store: 'A'
      }
    }
    const httpResponse = await sut.route(httpRequest)
    expect(httpResponse.statusCode).toBe(400)
    expect(httpResponse.body).toEqual(new MissingParamError('amount'))
  })

  test('Should return 400 if no store is provide', async () => {
    const { sut } = makeSut()
    const httpRequest = {
      body: {
        email: 'any_password@email.com',
        amount: 2000
      }
    }
    const httpResponse = await sut.route(httpRequest)
    expect(httpResponse.statusCode).toBe(400)
    expect(httpResponse.body).toEqual(new MissingParamError('store'))
  })

  test('Should return 400 if is invalid email is provide', async () => {
    const { sut, emailValidatorSpy } = makeSut()
    emailValidatorSpy.isEmailValid = false
    const httpRequest = {
      body: {
        email: 'invalid@perez.cl',
        amount: 2000,
        store: 'A'
      }
    }
    const httpResponse = await sut.route(httpRequest)
    expect(httpResponse.statusCode).toBe(400)
    expect(httpResponse.body).toEqual(new InvalidParamError('email'))
  })

  test('Should returnm 500 if no authUseCase is provided', async () => {
    const sut = new AddRouter()
    const httpRequest = {
      body: {
        email: 'juan@perez.cl',
        amount: 2000,
        store: 'A'
      }
    }
    const httpResponse = await sut.route(httpRequest)
    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new ServerError())
  })

  test('Should returnm 500 if authUseCase has no auth method', async () => {
    const sut = new AddRouter({})
    const httpRequest = {
      body: {
        email: 'juan@perez.cl',
        amount: 2000,
        store: 'A'
      }
    }
    const httpResponse = await sut.route(httpRequest)
    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new ServerError())
  })

  test('Should returnm 500 if authUseCase throws', async () => {
    const addUseCaseSpy = makeAddUseCaseError()
    const sut = new AddRouter(addUseCaseSpy)
    const httpRequest = {
      body: {
        email: 'juan@perez.cl',
        amount: 2000,
        store: 'A'
      }
    }
    const httpResponse = await sut.route(httpRequest)

    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new ServerError())
  })

  test('Should returnm 500 if no emailValidator is provided', async () => {
    const addUseCaseSpy = makeAddUseCase()
    const sut = new AddRouter(addUseCaseSpy)
    const httpRequest = {
      body: {
        email: 'juan@perez.cl',
        amount: 2000,
        store: 'A'
      }
    }
    const httpResponse = await sut.route(httpRequest)

    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new ServerError())
  })

  test('Should returnm 500 if emailValidator has no isValid Method', async () => {
    const addhUseCaseSpy = makeAddUseCase()
    const sut = new AddRouter(addhUseCaseSpy, {})
    const httpRequest = {
      body: {
        email: 'juan@perez.cl',
        amount: 2000,
        store: 'A'
      }
    }
    const httpResponse = await sut.route(httpRequest)

    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new ServerError())
  })

  test('Should returnm 500 if emailValidator throws', async () => {
    const addUseCaseSpy = makeAddUseCaseError()
    const emailValidatorSpy = makeEmailValidatorError()
    const sut = new AddRouter(addUseCaseSpy, emailValidatorSpy)
    const httpRequest = {
      body: {
        email: 'juan@perez.cl',
        amount: 2000,
        store: 'A'
      }
    }
    const httpResponse = await sut.route(httpRequest)

    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new ServerError())
  })

  test('Should return 500 if no httpRequest is provide', async () => {
    const { sut } = makeSut()
    const httpResponse = await sut.route()
    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new ServerError())
  })

  test('Should return 500 if httpRequest has no body', async () => {
    const { sut } = makeSut()
    const httpResponse = await sut.route({})
    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new ServerError())
  })
})
