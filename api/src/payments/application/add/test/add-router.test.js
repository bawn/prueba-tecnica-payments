const AddUseCase = require('../../../domain/add/useCase/add-useCase')
const EmailValidator = require('../../../../main/application/emailValidator/utils/email-validator')
const LoadUserByEmailAndStoreRepository = require('../../../infrastructure/load/repositories/loadPayment-repository')
const AddRouter = require('../router/add-router')

const makeSut = () => {
  const emailValidator = new EmailValidator()
  const loadUserbyEmailAndStoreRepository = new LoadUserByEmailAndStoreRepository()
  const addUseCase = new AddUseCase({ loadUserbyEmailAndStoreRepository })
  const sut = new AddRouter({ addUseCase, emailValidator })
  return { sut, addUseCase, emailValidator }
}

describe('Payments Use Case', () => {
  test('Should return payment if an valid data is provided', async () => {
    const { sut } = makeSut()
    const httpRequest = {
      body: {
        email: 'juan@perez.cl',
        amount: 1000,
        store: 'A'
      }
    }
    const payment = await sut.route(httpRequest)
    expect(payment).toBeTruthy()
  })
})
