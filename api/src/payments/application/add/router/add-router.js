const httpResponse = require('../../../../main/domain/helpers/http-reponse')
const { MissingParamError, InvalidParamError } = require('../../../../main/domain/errors/index')

module.exports = class AddRouter {
  constructor ({ addUseCase, emailValidator } = {}) {
    this.emailValidator = emailValidator
    this.addUseCase = addUseCase
  }

  async route (httpRequest) {
    try {
      const { email, amount, store } = httpRequest.body
      if (!email) {
        return httpResponse.badRequest(new MissingParamError('email'))
      }

      if (!amount) {
        return httpResponse.badRequest(new MissingParamError('amount'))
      }

      if (!store) {
        return httpResponse.badRequest(new MissingParamError('store'))
      }

      if (!this.emailValidator.isValid(email)) {
        return httpResponse.badRequest(new InvalidParamError('email'))
      }

      const message = await this.addUseCase.add(email, amount, store)

      return httpResponse.Ok({ message })
    } catch (error) {
      return httpResponse.serverError()
    }
  }
}
