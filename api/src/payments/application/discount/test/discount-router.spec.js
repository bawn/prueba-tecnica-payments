const DiscountRouter = require('../router/discount-router')
const { MissingParamError, InvalidParamError, ServerError } = require('../../../../main/domain/errors/index')

const makeSut = () => {
  const discountUseCaseSpy = makeDiscountCase()
  const emailValidatorSpy = makeEmailValidator()
  const sut = new DiscountRouter({
    discountUseCase: discountUseCaseSpy,
    emailValidator: emailValidatorSpy
  })
  return { sut, discountUseCaseSpy, emailValidatorSpy }
}

const makeDiscountCase = () => {
  class DiscountUseCaseSpy {
    async discount (email, amount, store) {
      this.email = email
      this.store = store
      this.amount = amount

      this.menssage = {
        email, store, amount
      }

      return this.menssage
    }
  }

  return new DiscountUseCaseSpy()
}

const makeDiscountUseCaseError = () => {
  class DiscountUseCaseSpy {
    async discount () {
      throw new Error()
    }
  }

  return new DiscountUseCaseSpy()
}

const makeEmailValidator = () => {
  class EmailValidatorSpy {
    isValid (email) {
      this.email = email
      return this.isEmailValid
    }
  }

  const emailValidatorSpy = new EmailValidatorSpy()
  emailValidatorSpy.isEmailValid = true
  return emailValidatorSpy
}

const makeEmailValidatorError = () => {
  class EmailValidatorSpy {
    isValid () {
      throw new Error()
    }
  }

  return new EmailValidatorSpy()
}

describe('Discount Router', () => {
  test('Should return 200 Discount amount store', async () => {
    const { sut, discountUseCaseSpy } = makeSut()
    const httpRequest = {
      body: {
        email: 'juan@perez.cl',
        store: 'A',
        amount: 1000
      }
    }
    const httpResponse = await sut.route(httpRequest)
    expect(httpResponse.statusCode).toBe(200)
    expect(httpResponse.body).toEqual(discountUseCaseSpy.menssage)
  })

  test('Should call EmailValidator with correct email', async () => {
    const { sut, emailValidatorSpy } = makeSut()
    const httpRequest = {
      body: {
        email: 'juan@perez.cl',
        amount: 1000,
        store: 'A'
      }
    }
    await sut.route(httpRequest)
    expect(emailValidatorSpy.email).toBe(httpRequest.body.email)
  })

  test('Should call DiscountUseCase with correct params', async () => {
    const { sut, discountUseCaseSpy } = makeSut()
    const httpRequest = {
      body: {
        email: 'any_email@email.com',
        store: 'A',
        amount: 1000
      }
    }
    await sut.route(httpRequest)
    expect(discountUseCaseSpy.email).toBe(httpRequest.body.email)
    expect(discountUseCaseSpy.store).toBe(httpRequest.body.store)
    expect(discountUseCaseSpy.amount).toBe(httpRequest.body.amount)
  })

  test('Should return 400 if no email is provide', async () => {
    const { sut } = makeSut()
    const httpRequest = {
      body: {
        amount: 1000,
        store: 'A'
      }
    }
    const httpResponse = await sut.route(httpRequest)

    expect(httpResponse.statusCode).toBe(400)
    expect(httpResponse.body).toEqual(new MissingParamError('email'))
  })

  test('Should return 400 if no store is provide', async () => {
    const { sut } = makeSut()
    const httpRequest = {
      body: {
        email: 'any_password@email.com',
        amount: 1000
      }
    }
    const httpResponse = await sut.route(httpRequest)
    expect(httpResponse.statusCode).toBe(400)
    expect(httpResponse.body).toEqual(new MissingParamError('store'))
  })

  test('Should return 400 if is invalid email is provide', async () => {
    const { sut, emailValidatorSpy } = makeSut()
    emailValidatorSpy.isEmailValid = false
    const httpRequest = {
      body: {
        email: 'invalid@perez.cl',
        amount: 1000,
        store: 'A'
      }
    }
    const httpResponse = await sut.route(httpRequest)
    expect(httpResponse.statusCode).toBe(400)
    expect(httpResponse.body).toEqual(new InvalidParamError('email'))
  })

  test('Should returnm 500 if no DiscountRouterUseCase is provided', async () => {
    const sut = new DiscountRouter()
    const httpRequest = {
      body: {
        email: 'juan@perez.cl',
        amount: 1000,
        store: 'A'
      }
    }
    const httpResponse = await sut.route(httpRequest)
    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new ServerError())
  })

  test('Should returnm 500 if UseCase has no discount method', async () => {
    const sut = new DiscountRouter({})
    const httpRequest = {
      body: {
        email: 'juan@perez.cl',
        amount: 1000,
        store: 'A'
      }
    }
    const httpResponse = await sut.route(httpRequest)
    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new ServerError())
  })

  test('Should returnm 500 if discountUseCase throws', async () => {
    const discountUseCaseSpy = makeDiscountUseCaseError()
    const sut = new DiscountRouter(discountUseCaseSpy)
    const httpRequest = {
      body: {
        email: 'juan@perez.cl',
        amount: 1000,
        store: 'A'
      }
    }
    const httpResponse = await sut.route(httpRequest)

    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new ServerError())
  })

  test('Should returnm 500 if no emailValidator is provided', async () => {
    const discountUseCaseSpy = makeDiscountCase()
    const sut = new DiscountRouter(discountUseCaseSpy)
    const httpRequest = {
      body: {
        email: 'juan@perez.cl',
        amount: 1000,
        store: 'A'
      }
    }
    const httpResponse = await sut.route(httpRequest)

    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new ServerError())
  })

  test('Should returnm 500 if emailValidator has no isValid Method', async () => {
    const discountUseCaseSpy = makeDiscountCase()
    const sut = new DiscountRouter(discountUseCaseSpy, {})
    const httpRequest = {
      body: {
        email: 'juan@perez.cl',
        amount: 1000,
        store: 'A'
      }
    }
    const httpResponse = await sut.route(httpRequest)

    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new ServerError())
  })

  test('Should returnm 500 if emailValidator throws', async () => {
    const discountUseCaseSpy = makeDiscountUseCaseError()
    const emailValidatorSpy = makeEmailValidatorError()
    const sut = new DiscountRouter(discountUseCaseSpy, emailValidatorSpy)
    const httpRequest = {
      body: {
        email: 'juan@perez.cl',
        amount: 1000,
        store: 'A'
      }
    }
    const httpResponse = await sut.route(httpRequest)

    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new ServerError())
  })

  test('Should return 500 if no httpRequest is provide', async () => {
    const { sut } = makeSut()
    const httpResponse = await sut.route()
    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new ServerError())
  })

  test('Should return 500 if httpRequest has no body', async () => {
    const { sut } = makeSut()
    const httpResponse = await sut.route({})
    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new ServerError())
  })
})
