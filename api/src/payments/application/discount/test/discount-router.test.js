const DiscountUseCase = require('../../../domain/discount/useCase/discount-useCase')
const EmailValidator = require('../../../../main/application/emailValidator/utils/email-validator')
const LoadUserByEmailAndStoreRepository = require('../../../infrastructure/load/repositories/loadPayment-repository')
const WriteUserByEmailAndStoreRepository = require('../../../infrastructure/write/repositories/writePayment-repository')
const DiscountRouter = require('../router/discount-router')

const makeSut = () => {
  const emailValidator = new EmailValidator()
  const loadUserbyEmailAndStoreRepository = new LoadUserByEmailAndStoreRepository()
  const writeUserByEmailAndStoreRepository = new WriteUserByEmailAndStoreRepository()
  const discountUseCase = new DiscountUseCase({ loadUserbyEmailAndStoreRepository, writeUserByEmailAndStoreRepository })
  const sut = new DiscountRouter({ discountUseCase, emailValidator })
  return { sut, discountUseCase, emailValidator }
}

describe('Payments Use Case', () => {
  test('Should return payment if an valid data is provided', async () => {
    const { sut } = makeSut()
    const httpRequest = {
      body: {
        email: 'juan@perez.cl',
        amount: 1000,
        store: 'A'
      }
    }
    const payment = await sut.route(httpRequest)
    expect(payment).toBeTruthy()
  })
})
