const httpResponse = require('../../../../main/domain/helpers/http-reponse')
const { MissingParamError, InvalidParamError } = require('../../../../main/domain/errors/index')

module.exports = class DiscountRouter {
  constructor ({ discountUseCase, emailValidator } = {}) {
    this.emailValidator = emailValidator
    this.discountUseCase = discountUseCase
  }

  async route (httpRequest) {
    try {
      const { email, amount, store } = httpRequest.body
      if (!email) {
        return httpResponse.badRequest(new MissingParamError('email'))
      }

      if (!amount) {
        return httpResponse.badRequest(new MissingParamError('amount'))
      }

      if (!store) {
        return httpResponse.badRequest(new MissingParamError('store'))
      }

      if (!this.emailValidator.isValid(email)) {
        return httpResponse.badRequest(new InvalidParamError('email'))
      }

      const message = await this.discountUseCase.discount(email, amount, store)

      return httpResponse.Ok({ message })
    } catch (error) {
      return httpResponse.serverError()
    }
  }
}
