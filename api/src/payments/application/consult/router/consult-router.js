const httpResponse = require('../../../../main/domain/helpers/http-reponse')
const { MissingParamError, InvalidParamError } = require('../../../../main/domain/errors/index')

module.exports = class ConsultRouter {
  constructor ({ consultUseCase, emailValidator } = {}) {
    this.emailValidator = emailValidator
    this.consultUseCase = consultUseCase
  }

  async route (httpRequest) {
    try {
      const { email, store } = httpRequest.body
      if (!email) {
        return httpResponse.badRequest(new MissingParamError('email'))
      }

      if (!store) {
        return httpResponse.badRequest(new MissingParamError('store'))
      }

      if (!this.emailValidator.isValid(email)) {
        return httpResponse.badRequest(new InvalidParamError('email'))
      }

      const message = await this.consultUseCase.consult(email, store)

      return httpResponse.Ok({ message })
    } catch (error) {
      return httpResponse.serverError()
    }
  }
}
