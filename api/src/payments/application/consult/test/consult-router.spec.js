const ConsultRouter = require('../router/consult-router')
const { MissingParamError, InvalidParamError, ServerError } = require('../../../../main/domain/errors/index')

const makeSut = () => {
  const consultUseCaseSpy = makeConsultCase()
  const emailValidatorSpy = makeEmailValidator()
  const sut = new ConsultRouter({
    consultUseCase: consultUseCaseSpy,
    emailValidator: emailValidatorSpy
  })
  return { sut, consultUseCaseSpy, emailValidatorSpy }
}

const makeConsultCase = () => {
  class ConsultUseCaseSpy {
    async consult (email, store) {
      this.email = email
      this.store = store

      this.menssage = {
        email, store
      }

      return this.menssage
    }
  }

  return new ConsultUseCaseSpy()
}

const makeConsultUseCaseError = () => {
  class ConsultUseCaseSpy {
    async consult () {
      throw new Error()
    }
  }

  return new ConsultUseCaseSpy()
}

const makeEmailValidator = () => {
  class EmailValidatorSpy {
    isValid (email) {
      this.email = email
      return this.isEmailValid
    }
  }

  const emailValidatorSpy = new EmailValidatorSpy()
  emailValidatorSpy.isEmailValid = true
  return emailValidatorSpy
}

const makeEmailValidatorError = () => {
  class EmailValidatorSpy {
    isValid () {
      throw new Error()
    }
  }

  return new EmailValidatorSpy()
}

describe('Consult Router', () => {
  test('Should return 200 Consult amount store', async () => {
    const { sut, consultUseCaseSpy } = makeSut()
    const httpRequest = {
      body: {
        email: 'juan@perez.cl',
        store: 'A'
      }
    }
    const httpResponse = await sut.route(httpRequest)
    expect(httpResponse.statusCode).toBe(200)
    expect(httpResponse.body).toEqual(consultUseCaseSpy.menssage)
  })

  test('Should call EmailValidator with correct email', async () => {
    const { sut, emailValidatorSpy } = makeSut()
    const httpRequest = {
      body: {
        email: 'juan@perez.cl',
        store: 'A'
      }
    }
    await sut.route(httpRequest)
    expect(emailValidatorSpy.email).toBe(httpRequest.body.email)
  })

  test('Should call ConsultUseCase with correct params', async () => {
    const { sut, consultUseCaseSpy } = makeSut()
    const httpRequest = {
      body: {
        email: 'any_email@email.com',
        store: 'A'
      }
    }
    await sut.route(httpRequest)
    expect(consultUseCaseSpy.email).toBe(httpRequest.body.email)
    expect(consultUseCaseSpy.store).toBe(httpRequest.body.store)
  })

  test('Should return 400 if no email is provide', async () => {
    const { sut } = makeSut()
    const httpRequest = {
      body: {
        store: 'A'
      }
    }
    const httpResponse = await sut.route(httpRequest)

    expect(httpResponse.statusCode).toBe(400)
    expect(httpResponse.body).toEqual(new MissingParamError('email'))
  })

  test('Should return 400 if no store is provide', async () => {
    const { sut } = makeSut()
    const httpRequest = {
      body: {
        email: 'any_password@email.com'
      }
    }
    const httpResponse = await sut.route(httpRequest)
    expect(httpResponse.statusCode).toBe(400)
    expect(httpResponse.body).toEqual(new MissingParamError('store'))
  })

  test('Should return 400 if is invalid email is provide', async () => {
    const { sut, emailValidatorSpy } = makeSut()
    emailValidatorSpy.isEmailValid = false
    const httpRequest = {
      body: {
        email: 'invalid@perez.cl',
        store: 'A'
      }
    }
    const httpResponse = await sut.route(httpRequest)
    expect(httpResponse.statusCode).toBe(400)
    expect(httpResponse.body).toEqual(new InvalidParamError('email'))
  })

  test('Should returnm 500 if no ConsultUseCase is provided', async () => {
    const sut = new ConsultRouter()
    const httpRequest = {
      body: {
        email: 'juan@perez.cl',
        store: 'A'
      }
    }
    const httpResponse = await sut.route(httpRequest)
    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new ServerError())
  })

  test('Should returnm 500 if UseCase has no consult method', async () => {
    const sut = new ConsultRouter({})
    const httpRequest = {
      body: {
        email: 'juan@perez.cl',
        store: 'A'
      }
    }
    const httpResponse = await sut.route(httpRequest)
    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new ServerError())
  })

  test('Should returnm 500 if consultUseCase throws', async () => {
    const consultUseCaseSpy = makeConsultUseCaseError()
    const sut = new ConsultRouter(consultUseCaseSpy)
    const httpRequest = {
      body: {
        email: 'juan@perez.cl',
        store: 'A'
      }
    }
    const httpResponse = await sut.route(httpRequest)

    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new ServerError())
  })

  test('Should returnm 500 if no emailValidator is provided', async () => {
    const consultUseCaseSpy = makeConsultCase()
    const sut = new ConsultRouter(consultUseCaseSpy)
    const httpRequest = {
      body: {
        email: 'juan@perez.cl',
        store: 'A'
      }
    }
    const httpResponse = await sut.route(httpRequest)

    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new ServerError())
  })

  test('Should returnm 500 if emailValidator has no isValid Method', async () => {
    const consultUseCaseSpy = makeConsultCase()
    const sut = new ConsultRouter(consultUseCaseSpy, {})
    const httpRequest = {
      body: {
        email: 'juan@perez.cl',
        store: 'A'
      }
    }
    const httpResponse = await sut.route(httpRequest)

    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new ServerError())
  })

  test('Should returnm 500 if emailValidator throws', async () => {
    const consultUseCaseSpy = makeConsultUseCaseError()
    const emailValidatorSpy = makeEmailValidatorError()
    const sut = new ConsultRouter(consultUseCaseSpy, emailValidatorSpy)
    const httpRequest = {
      body: {
        email: 'juan@perez.cl',
        store: 'A'
      }
    }
    const httpResponse = await sut.route(httpRequest)

    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new ServerError())
  })

  test('Should return 500 if no httpRequest is provide', async () => {
    const { sut } = makeSut()
    const httpResponse = await sut.route()
    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new ServerError())
  })

  test('Should return 500 if httpRequest has no body', async () => {
    const { sut } = makeSut()
    const httpResponse = await sut.route({})
    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new ServerError())
  })
})
