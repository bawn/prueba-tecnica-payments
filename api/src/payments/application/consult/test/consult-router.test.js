const ConsultUseCase = require('../../../domain/consult/useCase/consult-useCase')
const EmailValidator = require('../../../../main/application/emailValidator/utils/email-validator')
const LoadUserByEmailAndStoreRepository = require('../../../infrastructure/load/repositories/loadPayment-repository')
const ConsultRouter = require('../router/consult-router')

const makeSut = () => {
  const emailValidator = new EmailValidator()
  const loadUserbyEmailAndStoreRepository = new LoadUserByEmailAndStoreRepository()
  const consultUseCase = new ConsultUseCase({ loadUserbyEmailAndStoreRepository })
  const sut = new ConsultRouter({ consultUseCase, emailValidator })
  return { sut, consultUseCase, emailValidator }
}

describe('Payments Use Case', () => {
  test('Should return payment if an valid data is provided', async () => {
    const { sut } = makeSut()
    const httpRequest = {
      body: {
        email: 'juan@perez.cl',
        store: 'A'
      }
    }
    const payment = await sut.route(httpRequest)
    expect(payment).toBeTruthy()
  })
})
