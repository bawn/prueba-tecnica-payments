const { MissingParamError } = require('../../../../main/domain/errors/index')

module.exports = class DiscountUseCase {
  constructor ({ loadUserbyEmailAndStoreRepository, writeUserbyEmailAndStoreRepository } = {}) {
    this.loadUserbyEmailAndStoreRepository = loadUserbyEmailAndStoreRepository
    this.writeUserbyEmailAndStoreRepository = writeUserbyEmailAndStoreRepository
  }

  async discount (email, amount, store) {
    if (!email) {
      throw new MissingParamError('email')
    }

    if (!amount || isNaN(parseInt(amount))) {
      throw new MissingParamError('amount')
    }

    if (!store) {
      throw new MissingParamError('store')
    }

    const load = await this.loadUserbyEmailAndStoreRepository.load(email, store)

    if (!load) {
      return null
    }

    if (parseInt(amount) >= parseInt(load.amount)) {
      amount = parseInt(amount) - parseInt(load.amount)
    }

    if (parseInt(amount) <= parseInt(load.amount)) {
      amount = parseInt(load.amount) - parseInt(amount)
    }

    const write = await this.writeUserbyEmailAndStoreRepository.write({ email, amount, store })

    if (!write) {
      return null
    }

    return write
  }
}
