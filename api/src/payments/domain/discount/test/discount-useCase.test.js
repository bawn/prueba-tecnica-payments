const LoadUserByEmailAndStoreRepository = require('../../../infrastructure/load/repositories/loadPayment-repository')
const WriteUserByEmailAndStoreRepository = require('../../../infrastructure/write/repositories/writePayment-repository')
const DiscountUseCase = require('../useCase/discount-useCase')

const makeLoadUserByEmailAndStoreRepository = () => {
  const loadUserbyEmailAndStoreRepository = new LoadUserByEmailAndStoreRepository()
  return loadUserbyEmailAndStoreRepository
}

const makeWriteUserByEmailAndStoreRepository = () => {
  const writeUserbyEmailAndStoreRepositorySpy = new WriteUserByEmailAndStoreRepository()
  return writeUserbyEmailAndStoreRepositorySpy
}

const makeSut = () => {
  const loadUserbyEmailAndStoreRepository = makeLoadUserByEmailAndStoreRepository()
  const writeUserbyEmailAndStoreRepository = makeWriteUserByEmailAndStoreRepository()
  const sut = new DiscountUseCase({ loadUserbyEmailAndStoreRepository, writeUserbyEmailAndStoreRepository })
  return sut
}

describe('Payments Use Case', () => {
  test('Should if null repositori is provided', async () => {
    const sut = new DiscountUseCase()
    const promise = sut.discount('juan@perez.cl', 1000, 'A')
    expect(promise).rejects.toThrow()
  })

  test('Should return null if an invalid email is provided', async () => {
    const sut = makeSut()
    const promise = await sut.discount('invalid_email@mail.com', 1000, 'A')
    expect(promise).toBeNull()
  })

  test('Should return null if an invalid store is provided', async () => {
    const sut = makeSut()
    const promise = await sut.discount('juan@perez.cl', 1000, 'F')
    expect(promise).toBeNull()
  })

  test('Should return payment if an valid data is provided', async () => {
    const sut = makeSut()
    const payment = await sut.discount('juan@perez.cl', 1000, 'A')
    expect(payment).toBeTruthy()
  })
})
