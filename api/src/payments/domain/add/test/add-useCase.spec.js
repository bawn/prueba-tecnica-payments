const { MissingParamError } = require('../../../../main/domain/errors/index')
const AddUseCase = require('../useCase/add-useCase')

const makeLoadUserByEmailAndStoreRepositorySpy = () => {
  class LoadUserbyEmailAndStoreRepositorySpy {
    async load (email, store) {
      this.email = email
      this.store = store

      this.load = {
        email, store
      }

      return this.load
    }
  }
  return new LoadUserbyEmailAndStoreRepositorySpy()
}

const makeWriteUserByEmailAndStoreRepositorySpy = () => {
  class WriteUserbyEmailAndStoreRepositorySpy {
    async write ({ email, amount, store }) {
      this.email = email
      this.store = store
      this.store = amount

      this.write = {
        email, store, amount
      }

      return this.write
    }
  }
  return new WriteUserbyEmailAndStoreRepositorySpy()
}

const makeSut = () => {
  const loadUserbyEmailAndStoreRepository = makeLoadUserByEmailAndStoreRepositorySpy()
  const writeUserbyEmailAndStoreRepository = makeWriteUserByEmailAndStoreRepositorySpy()
  loadUserbyEmailAndStoreRepository.user = {
    email: 'valid-email@email.com',
    amount: 2000,
    store: 'A'
  }
  const sut = new AddUseCase({ loadUserbyEmailAndStoreRepository, writeUserbyEmailAndStoreRepository })
  return {
    sut,
    writeUserbyEmailAndStoreRepository
  }
}

describe('Payments Use Case', () => {
  test('Should throw if no email provider', async () => {
    const { sut } = makeSut()
    const promise = sut.add(null, 2000, 'A')
    expect(promise).rejects.toThrow(new MissingParamError('email'))
  })

  test('Should throw if no amount provider', async () => {
    const { sut } = makeSut()
    const promise = sut.add('any_email@email.com', null, 'A')
    expect(promise).rejects.toThrow(new MissingParamError('amount'))
  })

  test('Should throw if no store provider', async () => {
    const { sut } = makeSut()
    const promise = sut.add('any_email@email.com', 2000, null)
    expect(promise).rejects.toThrow(new MissingParamError('store'))
  })

  test('Should call LoadUserbyEmailRepository with correct email ', async () => {
    const { sut, writeUserbyEmailAndStoreRepository } = makeSut()
    await sut.add('any_email@email.com', 2000, 'A')
    expect(writeUserbyEmailAndStoreRepository.email).toBe('any_email@email.com')
  })

  test('Should if null repositori is provided', async () => {
    const sut = new AddUseCase()
    const promise = sut.add('any_email@email.com', 2000, 'A')
    expect(promise).rejects.toThrow()
  })

  test('Should if null repositori has no load method', async () => {
    const sut = new AddUseCase({})
    const promise = sut.add('any_email@email.com', 2000, 'A')
    expect(promise).rejects.toThrow()
  })

  test('Should return user if an valid data is provided', async () => {
    const { sut } = makeSut()
    const user = await sut.add('valid_email@email.com', 2000, 'A')
    expect(user).toBeTruthy()
  })
})
