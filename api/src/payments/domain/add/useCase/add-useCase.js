const { MissingParamError } = require('../../../../main/domain/errors/index')

module.exports = class AddUseCase {
  constructor ({ loadUserbyEmailAndStoreRepository, writeUserbyEmailAndStoreRepository } = {}) {
    this.loadUserbyEmailAndStoreRepository = loadUserbyEmailAndStoreRepository
    this.writeUserbyEmailAndStoreRepository = writeUserbyEmailAndStoreRepository
  }

  async add (email, amount, store) {
    if (!email) {
      throw new MissingParamError('email')
    }

    if (!amount || isNaN(parseInt(amount))) {
      throw new MissingParamError('amount')
    }

    if (!store) {
      throw new MissingParamError('store')
    }

    const load = await this.loadUserbyEmailAndStoreRepository.load(email, store)

    if (!load) {
      return null
    }

    amount = parseInt(amount) + parseInt(load.amount)

    const write = await this.writeUserbyEmailAndStoreRepository.write({ email, amount, store })

    if (!write) {
      return null
    }

    return write
  }
}
