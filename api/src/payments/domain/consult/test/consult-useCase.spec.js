const { MissingParamError } = require('../../../../main/domain/errors/index')
const ConsultUseCase = require('../useCase/consult-useCase')

const makeLoadUserByEmailAndStoreRepositorySpy = () => {
  class LoadUserbyEmailAndStoreRepositorySpy {
    async load (email, store) {
      this.email = email
      this.store = store

      this.user = {
        email: email,
        amount: 1000,
        store: store
      }

      return this.user
    }
  }
  return new LoadUserbyEmailAndStoreRepositorySpy()
}

const makeSut = () => {
  const loadUserbyEmailAndStoreRepositorySpy = makeLoadUserByEmailAndStoreRepositorySpy()
  loadUserbyEmailAndStoreRepositorySpy.message = {
    email: 'valid-email@email.com',
    amount: 2000,
    store: 'A'
  }
  const sut = new ConsultUseCase({ loadUserbyEmailAndStoreRepositorySpy })
  return {
    sut,
    loadUserbyEmailAndStoreRepositorySpy
  }
}

describe('Payments Use Case', () => {
  test('Should throw if no email provider', async () => {
    const { sut } = makeSut()
    const promise = sut.consult()
    expect(promise).rejects.toThrow(new MissingParamError('email'))
  })

  test('Should throw if no store provider', async () => {
    const { sut } = makeSut()
    const promise = sut.consult('any_email@email.com')
    expect(promise).rejects.toThrow(new MissingParamError('store'))
  })

  test('Should if null repositori is provided', async () => {
    const sut = new ConsultUseCase()
    const promise = sut.consult('any_email@email.com', 'A')
    expect(promise).rejects.toThrow()
  })

  test('Should if null repositori has no load method', async () => {
    const sut = new ConsultUseCase({})
    const promise = sut.consult('any_email@email.com', 'A')
    expect(promise).rejects.toThrow()
  })
})
