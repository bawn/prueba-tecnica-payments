const LoadUserByEmailAndStoreRepository = require('../../../infrastructure/load/repositories/loadPayment-repository')
const ConsultUseCase = require('../useCase/consult-useCase')

const makeLoadUserByEmailAndStoreRepository = () => {
  const loadUserbyEmailAndStoreRepositorySpy = new LoadUserByEmailAndStoreRepository()
  return loadUserbyEmailAndStoreRepositorySpy
}

const makeSut = () => {
  const loadUserbyEmailAndStoreRepository = makeLoadUserByEmailAndStoreRepository()
  const sut = new ConsultUseCase({ loadUserbyEmailAndStoreRepository })
  return {
    sut,
    loadUserbyEmailAndStoreRepository
  }
}

describe('Payments Use Case', () => {
  test('Should if null repositori is provided', async () => {
    const sut = new ConsultUseCase()
    const promise = sut.consult('juan@perez.cl', 'A')
    expect(promise).rejects.toThrow()
  })

  test('Should return null if an invalid email is provided', async () => {
    const { sut } = makeSut()
    const promise = await sut.consult('invalid_email@mail.com', 'A')
    expect(promise).toBeNull()
  })

  test('Should return null if an invalid store is provided', async () => {
    const { sut } = makeSut()
    const promise = await sut.consult('juan@perez.cl', 'F')
    expect(promise).toBeNull()
  })

  test('Should return payment if an valid data is provided', async () => {
    const { sut } = makeSut()
    const payment = await sut.consult('juan@perez.cl', 'A')
    expect(payment).toBeTruthy()
  })
})
