const { MissingParamError } = require('../../../../main/domain/errors/index')

module.exports = class ConsultUseCase {
  constructor ({ loadUserbyEmailAndStoreRepository } = {}) {
    this.loadUserbyEmailAndStoreRepository = loadUserbyEmailAndStoreRepository
  }

  async consult (email, store) {
    if (!email) {
      throw new MissingParamError('email')
    }

    if (!store) {
      throw new MissingParamError('store')
    }

    const message = await this.loadUserbyEmailAndStoreRepository.load(email, store)

    if (message) {
      return message
    }

    return null
  }
}
